import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck as QC
import qualified Test.Tasty.SmallCheck as SC

import Data.List
import Data.Ord()
import qualified Data.Map.Strict as Map

import Test.SmallCheck.Series()
import Types(Cell(EmptyCell, Red, Blue))
import ApiServices
import Api()
import Control.Lens


main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [properties, unitTests]

properties :: TestTree
properties = testGroup "Properties" [scProps, qcProps]

scProps :: TestTree
scProps = testGroup "(checked by SmallCheck)"
  [ SC.testProperty "sort == sort . reverse" $
      \list -> sort (list :: [Int]) == sort (reverse list)
  , SC.testProperty "Fermat's little theorem" $
      \x -> ((x :: Integer) ^ 7 - x) `mod` 7 == 0
  ]

qcProps :: TestTree
qcProps = testGroup "(checked by QuickCheck)"
  [ QC.testProperty "sort == sort . reverse" $
      \list -> sort (list :: [Int]) == sort (reverse list)
  , QC.testProperty "Fermat's little theorem" $
      \x -> ((x :: Integer)^7 - x) `mod` 7 == 0
  ]

unitTests :: TestTree
unitTests = testGroup "Unit tests"
  [ testCase "First element is EmptyCell with !!" $ head [EmptyCell, Red, Blue] @?= EmptyCell
  , testCase "Second element is EmptyCell" $ [EmptyCell, Red, Blue] !! 1 @?= Red
  , testCase "Third element is EmptyCell with Lens" $ case [EmptyCell, Red, Blue] ^? element 2 of
    Just blue -> blue @?= Blue
    Nothing -> EmptyCell @?= Blue
  , testCase "Replace First EmptyCell to Red" $ (element 0 .~ Red) [EmptyCell, Red, Blue] @?= [Red, Red, Blue]
  , testCase "Test to Equality 2 GameFields" $ [EmptyCell, Red, Blue] @?= [EmptyCell, Red, Blue]
  , testCase "Neighbours Indexes of 5th cell" $ [65, 75, 74] @?= neighboursIndexes 64
  , testCase "GameField to Graph" $ mapGameFieldToGraph (replicate 121 Blue) Blue @?= Map.fromList [(0,[1,11,10]),(1,[2,12,11]),(2,[3,13,12]),(3,[4,14,13]),
    (4,[5,15,14]),(5,[6,16,15]),(6,[7,17,16]),(7,[8,18,17]),(8,[9,19,18]),(9,[10,20,19]),(10,[11,21,20]),(11,[12,22,21]),(12,[13,23,22]),(13,[14,24,23]),
    (14,[15,25,24]),(15,[16,26,25]),(16,[17,27,26]),(17,[18,28,27]),(18,[19,29,28]),(19,[20,30,29]),(20,[21,31,30]),(21,[22,32,31]),(22,[23,33,32]),(23,[24,34,33]),
    (24,[25,35,34]),(25,[26,36,35]),(26,[27,37,36]),(27,[28,38,37]),(28,[29,39,38]),(29,[30,40,39]),(30,[31,41,40]),(31,[32,42,41]),(32,[33,43,42]),(33,[34,44,43]),
    (34,[35,45,44]),(35,[36,46,45]),(36,[37,47,46]),(37,[38,48,47]),(38,[39,49,48]),(39,[40,50,49]),(40,[41,51,50]),(41,[42,52,51]),(42,[43,53,52]),(43,[44,54,53]),
    (44,[45,55,54]),(45,[46,56,55]),(46,[47,57,56]),(47,[48,58,57]),(48,[49,59,58]),(49,[50,60,59]),(50,[51,61,60]),(51,[52,62,61]),(52,[53,63,62]),(53,[54,64,63]),
    (54,[55,65,64]),(55,[56,66,65]),(56,[57,67,66]),(57,[58,68,67]),(58,[59,69,68]),(59,[60,70,69]),(60,[61,71,70]),(61,[62,72,71]),(62,[63,73,72]),(63,[64,74,73]),
    (64,[65,75,74]),(65,[66,76,75]),(66,[67,77,76]),(67,[68,78,77]),(68,[69,79,78]),(69,[70,80,79]),(70,[71,81,80]),(71,[72,82,81]),(72,[73,83,82]),(73,[74,84,83]),
    (74,[75,85,84]),(75,[76,86,85]),(76,[77,87,86]),(77,[78,88,87]),(78,[79,89,88]),(79,[80,90,89]),(80,[81,91,90]),(81,[82,92,91]),(82,[83,93,92]),(83,[84,94,93]),
    (84,[85,95,94]),(85,[86,96,95]),(86,[87,97,96]),(87,[88,98,97]),(88,[89,99,98]),(89,[90,100,99]),(90,[91,101,100]),(91,[92,102,101]),(92,[93,103,102]),(93,[94,104,103]),
    (94,[95,105,104]),(95,[96,106,105]),(96,[97,107,106]),(97,[98,108,107]),(98,[99,109,108]),(99,[100,110,109]),(100,[101,111,110]),(101,[102,112,111]),(102,[103,113,112]),
    (103,[104,114,113]),(104,[105,115,114]),(105,[106,116,115]),(106,[107,117,116]),(107,[108,118,117]),(108,[109,119,118]),(109,[110,120,119]),(110,[111,120]),(111,[112]),
    (112,[113]),(113,[114]),(114,[115]),(115,[116]),(116,[117]),(117,[118]),(118,[119]),(119,[120]),(120,[])]
  	, testCase "redStartIndexes are equal" $ redStartIndexes @?= [0..10]
  	, testCase "blueStartIndexes are equal" $ blueStartIndexes @?= map (*11) [0..10]
  	, testCase "redEndIndexes are equal" $ redEndIndexes @?= map (\x -> 120 - x) [0..10]
  	, testCase "blueEndIndexes are equal" $ blueEndIndexes @?= (map (+10) $ map (*11) [0..10])
  ]
