module Main where

import Api
import Network.Wai.Handler.Warp
import Network.Wai
import Network.Wai.Middleware.Cors
import Network.HTTP.Types.Header
import qualified Data.Map.Strict as Map
import State
import Control.Concurrent.STM


corsPolicy :: Middleware
corsPolicy = cors (const $ Just policy)
    where
      policy = simpleCorsResourcePolicy 
        { corsOrigins = Nothing, corsRequestHeaders = [hContentType] }

-- TODO брать пользователей из файла
main :: IO ()
main = do
  initialUsers <- atomically $ newTVar Map.empty
  initialGames <- atomically $ newTVar Map.empty
  let appWithCors s = corsPolicy $ app s
  run 8081 $ appWithCors $ State initialUsers initialGames
