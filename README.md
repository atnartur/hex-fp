# hex-fp

## Команды

- `stack run` - запуск haskell

**Frontend**

- `cd frontend` - переход в папку с фронтендом
- `npm ci` - установка зависимостей
- `npm run serve` - запуск сервера для разработки
- `npm run build` - сборка

## API

- Метод регистрации игрока
```
curl -X POST http://localhost:8081/game -H 'Content-Type: application/json' --data '{"userName": "AAA"}'
```

```
POST http://localhost:8081/game
Content-type: application/json
POST_BODY:
{"userName": "AAA"}
```

- Получение игры по ID пользователя
```
curl http://localhost:8081/game?id=57afc2de-ca75-4ffb-be15-8a10c11abb39 -H 'Content-Type: application/json'
```

```
GET http://localhost:8081/game?id=a6cf4bac-85b2-4d94-8804-669d235b3483u
Content-type: application/json
```

- Сделать ход
```
curl -X POST http://localhost:8081/step -H 'Content-Type: application/json' --data '{"gameStepUserId": "af5acff7-e379-4de2-bbfb-ce3436bb9a38", "cellNumber": 1}' --verbose
```
