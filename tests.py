import requests

start_game_res = requests.post('http://localhost:8081/game', json={"userName": "AAA"})
print(start_game_res)

start_game_res_json = start_game_res.json()
print(start_game_res_json)
id = start_game_res_json['userId']

step_res = requests.post('http://localhost:8081/step', json={"gameStepUserId": id, "cellNumber": 1})
print(step_res)
print(step_res.content)

start_game_res2 = requests.post('http://localhost:8081/game', json={"userName": "AAA2"})
print(start_game_res2)

get_game_res = requests.get(f'http://localhost:8081/game?id={id}')
print(get_game_res)
get_game_res_json = get_game_res.json()
print(get_game_res_json)


step_res = requests.post('http://localhost:8081/step', json={"gameStepUserId": id, "cellNumber": 1})
print(step_res)
print(step_res.content)
