{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}


module ApiServices where

import Types
import State
import Data.UUID as UUID
import Data.Maybe
import Control.Monad.IO.Class
import Control.Monad.Trans.Reader (asks)
import Control.Concurrent.STM
import qualified Data.Map.Strict as Map
import Algorithm.Search


-- Получает пользователя из хранилища
getUser :: UUID -> AppM (Maybe User)
getUser localUserId = do
  usersState <- asks users
  liftIO $ atomically $ Map.lookup localUserId <$> readTVar usersState


-- Получает игру из хранилища
getGame :: UUID -> AppM (Maybe Game)
getGame localGameId = do
  gamesState <- asks games
  liftIO $ atomically $ Map.lookup localGameId <$> readTVar gamesState


-- Выясняет, какого цвета игрок с определенным ID внутри игры
getCurrentUserIdColor :: Game -> UUID -> Cell
getCurrentUserIdColor game userId = 
  case isJust (red (gameUsers game)) && fromJust (red (gameUsers game)) == userId of
    True -> Red
    False -> case isJust (blue (gameUsers game)) && fromJust (blue (gameUsers game)) == userId of
      True -> Blue
      False -> EmptyCell


-- Возвщарает индексы соседних ячеек относительно текущей ячейки
neighboursIndexes :: Num a => a -> [a]
neighboursIndexes index = [index - 1, index - 10, index - 11, index + 1, index + 11, index + 10]


-- Фильтрует ячейки по цвету и по логичности индексов (отсекает индексы за пределами поля)
filterCellsByColor :: GameField -> Cell -> Int -> Bool
filterCellsByColor gameField cell currentNeiIndex = currentNeiIndex >= 0 && currentNeiIndex < 121 && gameField!!currentNeiIndex == cell


-- Превращает игровое поле в Map, пригодный для передачи в алгоритм поиска графа
mapGameFieldToGraph :: GameField -> Cell -> Map.Map Int [Int]
mapGameFieldToGraph gameField cell = do
  let gameFieldWithIndexes = zip gameField [0..]
  let filteredField = filter (\(x, _) -> x == cell) gameFieldWithIndexes
  let graphConnections = map (\(_, index) -> (index, filter (filterCellsByColor gameField cell) (neighboursIndexes index))) filteredField
  Map.fromList graphConnections


-- Индексы для поиска путей в графах
redStartIndexes :: [Int]
redStartIndexes = [0..10]

redEndIndexes :: [Int]
redEndIndexes = map (\x -> 120 - x) redStartIndexes

blueStartIndexes :: [Int]
blueStartIndexes = map (*11) redStartIndexes

blueEndIndexes :: [Int]
blueEndIndexes = map (+10) blueStartIndexes

getFilteredStartIndexes :: GameField -> Cell -> [Int]
getFilteredStartIndexes gameField cell = filter (\ind -> gameField!!ind == cell) (if cell == Blue then blueStartIndexes else redStartIndexes)

getFilteredEndIndexes :: GameField -> Cell -> [Int]
getFilteredEndIndexes gameField cell = filter (\ind -> gameField!!ind == cell) (if cell == Blue then blueEndIndexes else redEndIndexes)


-- запускает поиск в графе. Обертка нужна, чтобы нормально воткнуть start-значение в Maybe
runDfs :: (Map.Map Int [Int]) -> Int -> Int -> Maybe [Int]
runDfs graph start end = do 
  let result = dfs (graph Map.!) (== end) start
  case result of
    -- (докидываем start в начало, чтобы потом красиво отобразить путь от начала и до конца)
    Just path -> Just (start : path)
    Nothing -> Nothing


-- Находит победный путь на поле для определенного цвета
findWinnerPath :: GameField -> Cell -> Maybe [Int]
findWinnerPath gameField cell = do
  -- взять стартовые индексы, отфильтровать индексы, где стоит нужный нам цвет
  let filteredStartIndexes = getFilteredStartIndexes gameField cell
  let filteredEndIndexes = getFilteredEndIndexes gameField cell

  if Prelude.null filteredStartIndexes || Prelude.null filteredEndIndexes
    then Nothing
    else do
      -- создаем граф всех переходов для этого цвета
      let graph = mapGameFieldToGraph gameField cell
      -- находим все комбинации старта и начала
      let startEndTuples = [(x, y) | x <- filteredStartIndexes, y <- filteredEndIndexes]
      -- находим все пути между ними 
      let paths = map (\(start, end) -> runDfs graph start end) startEndTuples
      -- убираем те комбинации, которые не нашлись
      let filteredPaths = filter isJust paths
      
      -- если комбинация есть, Just, иначе Nothing - на нашли.
      if Prelude.null filteredPaths
        then Nothing
        else filteredPaths!!0


-- возвращает IDшник победившего юзера и победный путь, если получилось определить победителя
getWinnerUserIdAndPath :: Game -> Maybe (UUID, [Int])
getWinnerUserIdAndPath game = do
  let field = gameField game
  let bluePath = findWinnerPath field Blue
  case bluePath of
    Just path -> do
      let userId = fromJust $ blue $ gameUsers game
      Just (userId, path)
    Nothing -> do
      let redPath = findWinnerPath field Red
      case redPath of
        Just path -> do
          let user2Id = fromJust $ red $ gameUsers game
          Just (user2Id, path)
        Nothing -> Nothing


