{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}


module Types where

import Data.UUID as UUID
import qualified Data.Map.Strict as Map
import GHC.Generics (Generic)
import Data.Aeson


-- Тип "Ячейки", которая может быть Синей, Красной и Серой.
data Cell
  = Blue
  | Red
  | EmptyCell
  deriving (Eq, Show, Read, Generic, Ord)

instance ToJSON Cell

-- Тип статуса игра
data Status
  = Waiting
  | Running
  | Finished
  deriving (Eq, Show, Read, Generic)

instance ToJSON Status

-- Тип пользователя игры
data GameUsers = GameUsers {
  red :: Maybe UUID,
  blue :: Maybe UUID
} deriving (Show, Generic, Read)

instance ToJSON GameUsers

-- Тип "Доска"
type GameField = [Cell]

-- Тип игры
data Game = Game {
  status :: Status,
  gameField :: GameField,
  gameUsers :: GameUsers,
  turnUserId :: UUID,
  winnerUserId :: Maybe UUID,
  finishedAt :: Maybe Integer,
  createdAt :: Integer,
  winnerPath :: Maybe [Int]
} deriving (Show, Generic, Read)

instance ToJSON Game

-- Тип пользователя
data User = User {
  name :: String,
  currentGameId :: UUID
} deriving (Show, Generic, Read)

-- Тип Списка игр
type Games = Map.Map UUID Game

-- Тип Списка пользователей
type Users = Map.Map UUID User
