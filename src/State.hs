{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module State where

import Types
import Control.Concurrent          ()
import Control.Concurrent.STM.TVar (TVar)
import Control.Exception           ()
import Control.Monad.IO.Class      ()
import Control.Monad.STM           ()
import Control.Monad.Trans.Reader  (ReaderT)
import Data.Aeson                  ()
import GHC.Generics                ()
import           Servant

data State = State
  { users :: TVar Users,
    games :: TVar Games
  }

type AppM = ReaderT State Handler