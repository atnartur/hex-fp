{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}


module Api where

import Types
import ApiTypes
import ApiServices
import State
import Data.Aeson()
import Data.UUID as UUID
import Data.UUID.V4
import Data.Maybe
import GHC.Generics()
import GHC.TypeLits()
import Servant
import Network.HTTP.Client()
import Servant.API()
import Servant.JS()
import Servant.Client()
import qualified Data.Map.Strict as Map
import Data.Time.Clock.POSIX (getPOSIXTime)
import Control.Lens

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Reader  (asks, runReaderT)
import Control.Concurrent.STM

type HexAPI = "game" :> ReqBody '[JSON] UserLoginRequest :> PostCreated '[JSON] UserIdObj
         :<|> "game" :> QueryParam "id" UUID :> Get '[JSON] Game
         :<|> "step" :> ReqBody '[JSON] GameStep :> PostCreated '[JSON] NoContent

server :: ServerT HexAPI AppM
server = createUserMethod
    :<|> getGameMethod
    :<|> gameStepMethod

  -- Метод регистрации пользователя в игре
  where createUserMethod :: UserLoginRequest -> AppM UserIdObj
        createUserMethod requestData = do
          newUserId <- liftIO nextRandom
          newGameId <- liftIO nextRandom
          
          -- создаем объект пользователя
          let userObj = User (userName requestData) newGameId
          usersState <- asks users
          liftIO $ atomically $ modifyTVar' usersState $ Map.insert newUserId userObj

          -- ищем игру, в которую еще можно добавить пользователя
          gamesState <- asks games
          foundGames <- liftIO $ atomically $ Map.filter (isNothing . blue . gameUsers) <$> readTVar gamesState

          (gameId, gameObj) <- case Map.minViewWithKey foundGames of
            Nothing -> do
              -- если не нашли - создаем новую
              let gameField = replicate 121 EmptyCell
              let gameUsersAAA = GameUsers (Just newUserId) Nothing
              startTime <- liftIO $ round . (* 1000) <$> getPOSIXTime
              liftIO $ print "attaching to new game"
              pure (newGameId, Game Waiting gameField gameUsersAAA newUserId Nothing Nothing startTime Nothing)
            Just ((key, foundGame), _) -> do
              -- если нашли, дополняем данные существующей игры
              liftIO $ print "attaching existing game"
              liftIO $ atomically $ modifyTVar' usersState $ Map.insert newUserId userObj { currentGameId = key }
              pure (key, foundGame { gameUsers = (gameUsers foundGame) { blue = Just newUserId }, status = Running })

          liftIO $ atomically $ modifyTVar' gamesState $ Map.insert gameId gameObj

          return (UserIdObj newUserId)


        -- метод получения игры
        getGameMethod :: Maybe UUID -> AppM Game
        getGameMethod maybeLocalUserId =
          case maybeLocalUserId of
            Nothing -> throwError err400
            Just localUserId -> do
              maybeCurrentUser <- getUser localUserId
              case maybeCurrentUser of
                Nothing -> throwError err404
                Just foundUser -> do
                  maybeCurrentGame <- getGame (currentGameId foundUser)
                  case maybeCurrentGame of
                    Just foundGame -> pure foundGame
                    Nothing -> throwError err500


        -- метод выполнения хода
        gameStepMethod :: GameStep -> AppM NoContent
        gameStepMethod requestData = do
          let currentUserId = gameStepUserId requestData
          let currentCellNumber = cellNumber requestData
          liftIO $ print currentUserId
          liftIO $ print currentCellNumber

          gamesState <- asks games
          maybeCurrentUser <- getUser currentUserId
          case maybeCurrentUser of
            Nothing -> throwError err404
            Just foundUser -> do
              let currentGameIdentificator = currentGameId foundUser
              maybeCurrentGame <- getGame currentGameIdentificator

              case maybeCurrentGame of
                Nothing -> throwError err500
                Just foundGame -> do
                  liftIO $ print(status foundGame)
                  case status foundGame of
                    Running ->
                      case gameField foundGame ^? element currentCellNumber of
                        Just EmptyCell -> do
                          liftIO $ print (gameField foundGame ^? element currentCellNumber)
                          liftIO $ print (gameField foundGame)
                          case turnUserId foundGame of
                            currentUserId -> do

                              -- меняем айдишник пользователя, который должен сделать хоод

                              case red (gameUsers foundGame) of
                                Just currentUserUUID
                                  | currentUserUUID == currentUserId ->
                                    case blue (gameUsers foundGame) of
                                      Just blueUserId -> do
                                        let newField = (element currentCellNumber .~ Red) (gameField foundGame)
                                        liftIO $ atomically $ modifyTVar' gamesState $ Map.insert currentGameIdentificator (foundGame { gameField = newField, turnUserId = blueUserId } )
                                      Nothing -> throwError err422
                                  | otherwise -> liftIO $ print ""
                                Nothing -> throwError err422

                              case blue (gameUsers foundGame) of
                                Just currentUserUUID
                                  | currentUserUUID == currentUserId ->
                                    case red (gameUsers foundGame) of
                                      Just redUserId -> do
                                        let newField = (element currentCellNumber .~ Blue) (gameField foundGame)
                                        liftIO $ atomically $ modifyTVar' gamesState $ Map.insert currentGameIdentificator (foundGame { gameField = newField, turnUserId = redUserId })
                                      Nothing -> throwError err422
                                  | otherwise -> liftIO $ print ""
                                Nothing -> throwError err422

                              -- запускаем проверку на завершение игры и завершаем игру, если это необходимо
                              maybeCurGame <- getGame currentGameIdentificator
                              case maybeCurGame of
                                Nothing -> throwError err404
                                Just newGame ->
                                  case getWinnerUserIdAndPath newGame of
                                    Nothing -> liftIO $ print "continuing gaming"
                                    Just (winnerId, winPath) ->
                                      liftIO $ atomically $ modifyTVar' gamesState $ Map.insert currentGameIdentificator (newGame { winnerUserId = Just winnerId, winnerPath = Just winPath, status = Finished })

                            _ -> throwError err422
                        _ -> throwError err422
                    _ -> throwError err422
          return NoContent

-- свидетельство (witness) для вашего Api
myApi :: Proxy HexAPI
myApi = Proxy

-- wai - web application interface
-- Application - тип приложения в wai
nt s x = runReaderT x s

app :: State -> Application
app s = serve myApi $ hoistServer myApi (nt s) server