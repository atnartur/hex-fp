{-# LANGUAGE DeriveGeneric #-}

module ApiTypes where

import Data.Aeson
import Data.UUID as UUID
import GHC.Generics

-- DTO для создания пользователя
newtype UserLoginRequest = UserLoginRequest {
  userName :: String
} deriving (Show, Generic, Read)

instance FromJSON UserLoginRequest

-- DTO, содержащее идентификатор пользовтеля
newtype UserIdObj = UserIdObj {
  userId :: UUID
} deriving (Show, Generic, Read)

instance ToJSON UserIdObj
instance FromJSON UserIdObj

-- DTO хода игры
data GameStep = GameStep {
  gameStepUserId :: UUID,
  cellNumber :: Int
} deriving (Show, Generic, Read)

instance FromJSON GameStep