import Enum from 'enum';

export const GAME_STATUS = new Enum({
    'Waiting': 'Waiting',
    'Running': 'Running',
    'Finished': 'Finished'
});


export const CELL_TYPE = new Enum({
    'Blue': 'Blue',
    'Red': 'Red',
    'EmptyCell': 'EmptyCell'
});
