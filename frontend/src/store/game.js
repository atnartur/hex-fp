import request from '../request';
import {CELL_TYPE, GAME_STATUS as GAME_STATE} from "../enums";
import {getCellNumberFromXY} from "../utils";

export default {
    namespaced: true,
    state() {
        return {
            game: null,
            isLoading: false
        };
    },
    mutations: {
        loading: (state, value = true) => state.isLoading = value,
        game: (state, game) => state.game = game,
    },
    actions: {
        async load({ state, commit, rootState }) {
            if (!state.game) // индикатор загрузки показываем только при первой загрузке
                commit('loading');

            try {
                const res = await request.get(`/game?id=${rootState.user.id}`);
                commit('loading', false);
                commit('game', res.data);
                return res;
            }
            catch(e) {
                commit('loading', false);
                throw e;
            }
        },
        async cellClick({dispatch, state, rootState}, {currentValue, rowKey, key}) {
            if (!state.game) {
                return;
            }
            const g = state.game;
            const canStep = g.status === GAME_STATE.Running.value &&
                g.turnUserId === rootState.user.id &&
                currentValue === CELL_TYPE.EmptyCell.value;
            console.log('check:', g.status, g.turnUserId, currentValue, '=>', canStep);
            if (canStep) {
                const cellNumber = getCellNumberFromXY(rowKey, key);
                const res = await request.post('/step', {gameStepUserId: rootState.user.id, cellNumber});
                dispatch('load');
                console.log(res)
            }
        }
    }
};
