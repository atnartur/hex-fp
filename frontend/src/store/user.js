import request from '../request';

export default {
    namespaced: true,
    state() {
        return {
            name: null,
            id: null,
            isLoading: false,
            error: null
        };
    },
    mutations: {
        loading: (state, value = true) => state.isLoading = value,
        error: (state, error) => state.error = error,
        id: (state, id) => state.id = id,
        name: (state, name) => state.name = name,
    },
    actions: {
        async login({ commit }, name) {
            commit('loading');
            commit('error', null);
            try {
                const res = await request.post('/game', {userName: name});
                // Ждем, пока хаскель осознает, что мы вошли, и только потом сообщаем об этом фронту
                setTimeout(() => {
                    commit('id', res.data.userId);
                    commit('loading', false);
                    commit('name', name);
                }, 500);
                return res;
            }
            catch(e) {
                commit('loading', false);
                commit('error', e);
            }
        },
        logout({commit}) {
            commit('name', null);
            commit('id', null);
            // даем айдишнику и имени очиститься, потом перезагружаем страницу
            setTimeout(() => location.reload());
        }
    },
    getters: {
        isLoggedIn: state => state.id !== null
    }
};
