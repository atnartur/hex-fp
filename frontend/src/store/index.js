import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import game from './game'
import {VuexPersistence} from "vuex-persist";

Vue.use(Vuex);

const userLocalStorage = new VuexPersistence({
    storage: window.localStorage,
    key: 'user',
    reducer: state => ({
        user: {
            id: state.user.id,
            name: state.user.name,
        }
    })
});


export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {user, game},
    plugins: [userLocalStorage.plugin]
})
