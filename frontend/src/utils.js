/**
 * Вычисление индекса на основе координат (ну вдруг решим поменять систему измерения)
 * @param x
 * @param y
 * @return {number} индекс
 */
export function getCellNumberFromXY(x, y) {
    return x * 11 + y;
}
